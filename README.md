# Show Immutalbe.List data in tables using jss for styling

### usage

file sheet.js

```jsx
export default {
  // default classNames
  DataTable: {
    // root
  },
  tr: {},
  trEven: {},
  trOdd: {},
  th: {},
  td: {},
  tdEmpty: {},
  trh: {},
  // your own classNames
  thumb: {},
};
```

file pageWithTable.js

```jsx
import React from 'react';
import DataTable from 'react-immutable-jss-data-table';
import sheet from './sheet.js';
// your jss injectSheet instance
import injectSheet from 'lib/sheet';
import tableColumns from './columns.jsx'

const columns = tableColumns.select('title', 'link', 'user', 'clickMe');

class MyPage extends React.PureComponent {
  render() {
    const { classes, list } = this.props;
    return (
      <div
        className={classes.container}
      >
        <DataTable
          classes={classes}
          columns={columns}
          items={list}
        />
      </div>
    );
  }
}

export default injectSheet(sheet)(MyPage);
```

#### define table columns

You can use `fieldPath: ['path', 'to', 'val', 'in', 'rowData']` (`rowData.getIn(fieldPath)` will be used),
or define your own `getValue` function, or even pass React component as `render: CustomCellComponent`.

```jsx
import React from 'react';
import I from 'immutable';
import { columnsSelector } from 'react-immutable-jss-data-table';

export const columns = {
  // use rowIndex:
  cell0: {
    getValue: ({ rowIndex }) => `# ${rowIndex}`,
  },
  // add extra class, it will be searched in classes prop or cellClasses prop or used as is
  cell1: {
    className: 'my-own-class-name',
  },
  cell2: {
    // you can cpecify how to extract data from entries (rowData.getIn(fieldPath))
    fieldPath: ['article', 'title'],
    // then you can change the value
    getValue: ({ value }) => `Title: ${value}`,
  },
  cell3: {
    // how to extract data from rowData,
    fieldPath: ['article', 'title'],
    // then you can change the value
    getValue: ({ value, rowData }) => `Title: ${value}`,
    // you can render cell using your own component
    render: ({ value, rowData }) => <li children={'changed value: ' + value} />,
    // React component also can be used
    render: CustomCellComponent,
  },
  // if you whant to show column titles in table, please use 'title' prop
  cell4: {
    fieldPath: ['someField'],
    // column title (th)
    title: 'colum title',
    // you can change default title, f.e. add items amout
    getValueTh: ({ value, items }) => `${value} (${itmes.size})`,
    // render th using your own component
    renderTh: hoToRenderTh,
  },
  // you can change cell inner content markup using getValue:
  link: {
    fieldPath: ['article', 'url'],
    getValue: ({ value }) => <Link to={value} />,
  },
  // use DataTable props (<DataTable user={user}/>)
  user: {
    fieldPath: ['userId'],
    getValue: ({ classes, rowData, value, rowIndex, user }) =>
      <div
        className={classes.thumb}
        children={users.getFullNameById(value)}
      />,
  },
  // change cell markup fully
  clickMe: {
    render: ({ className, classes, rowData, value, rowIndex, props }) =>
      <div
        className={className}
        children={value}
        onClick={() => props.toggleEditRow(rowIndex)}
      />
  },
  // see definition bellow
  select: null,
};
columns.select = columnsSelector(columns);

export default columns;
```

#### available options for columns:

 * `fieldPath` - array used for Immutable.getIn method to extract value from rowData
 * `className` - custom className, used as additional className for cell
 * `getValue` - func, returns node or string or number, that will be placed as child in cell node
 * `getValueTh` - func, returns node or string or number, that will be placed as child in th node
 * `render` - func returns node, replaces cell root node
 * `title` - will be used as column title (like table > th) if showColumnsTitles set to true
 * `getValueTh` - allows to set column title using columnDef, and all DataTable props
 * `renderTh` - allows to render column, used as title for table, with your own React component

see [properties](#properties) for more details

You can set your own component to render rows, or decide wich one to use

```jsx
import DataTable, { Row } from 'react-immutable-jss-data-table';
const getRowRenderer = (props) => {
  const { rowData, rowIndex, ...allDataTableProps } = props;
  const Renderer = rowIndex === customRowIndex ? MyCustomRow : Row;
  return <Renderer {...props} />
}
<DataTable
  // ...
  Row={getRowRenderer}
/>
```

Customise empty row component, default RowEmpty can be used for your purposes

```jsx
import DataTable, { RowEmpty } from 'react-immutable-jss-data-table';

const CustomRowEmpty = (props) =>
  rowIndex == 1 ? <MyEmptyRowComponent /> : <RowEmpty {...props} />;

<DataTable
  // ...
  RowEmpty={CustomRowEmpty}
/>
```

set your own Th / Cell components
```jsx
import DataTable, { Th, Cell } from 'react-immutable-jss-data-table';

<DataTable
  // ...
  Th={Th}
  Cell={Cell}
/>
```

### Exported components and helpers

```jsx
import DataTable, {
  // default getters
  renderTh,
  getValueTh,
  getValue,
  renderCell,
  // components
  Th,
  Row,
  RowEmpty,
  Cell,
  DataTable,
  // sheet example
  sheet,
  // helpers
  getCellClassNames,
  getThClassNames,
  getRowClassNames,
  getColumnDef,
  columnsSelector,
} from 'react-immutable-jss-data-table';
```


### properties

component        | prop              | type       | description
-----------------|-------------------|------------|------------
DataTable        | &nbsp;            | &nbsp;     | &nbsp;
&nbsp;           | columns           | array      | array with [column definitions](#available-options-for-columns)
&nbsp;           | items             | I.List     | Immutable.List with data for table
&nbsp;           | classes           | object     | [jss classes](#jss-classes)
&nbsp;           | getRowClassNames  | function   | return array with classnames
&nbsp;           | getCellClassNames | function   | return array with classnames
&nbsp;           | getThClassNames   | function   | return array with classnames
&nbsp;           | showColumnsTitles | boolean    | display row.trh with titles.th
&nbsp;           | className         | string     | additional table className
&nbsp;           | Cell              | component  | component | function
&nbsp;           | Th                | component  | component | function
&nbsp;           | RowEmtpy          | component  | component | function
&nbsp;           | Row               | component  | component | function
&nbsp;           | renderTh          | renderable | default fn for `columnDef.renderTh`
&nbsp;           | getValueTh        | renderable | default fn for `columnDef.getValueTh`
&nbsp;           | getValue          | renderable | default fn for `columnDef.getValue` for Cell
&nbsp;           | render            | renderable | default fn for `columnDef.render` for Cell
&nbsp;           | defaultValue      | any        | defaultValue for Cell
Row              | &nbsp;            | &nbsp;     | &nbsp;
&nbsp;           | rowIndex          | integer    | 
&nbsp;           | rowData           | I.Map      | Immutable.Map with data for row
&nbsp;           | DataTable props   |            | 
Th               | &nbsp;            | &nbsp;     | &nbsp;
&nbsp;           | title             |            | `columnDef.title`
&nbsp;           | columnDef         |            | `columns.map(columnDef => ...)` [column definition](#available-options-for-columns)
&nbsp;           | getValueTh        |            | `columnDef.getValueTh`
&nbsp;           | renderTh          |            | `columnDef.renderTh`
&nbsp;           | DataTable props   |            | 
Cell             | &nbsp;            | &nbsp;     | &nbsp;
&nbsp;           | columnDef         | object     | `columns.map(columnDef => ...)` [column definition](#available-options-for-columns)
&nbsp;           | value             | any        | [extracted by](#columndef-getvalue)
&nbsp;           | rowData           | I.Map      | `items.map(rowData => ...)`
&nbsp;           | rowIndex          | integer    | 
&nbsp;           | render            | component  | 
&nbsp;           | getValue          | component  | [columnDef getValue](#columndef-getvalue)
&nbsp;           | fieldPath         | array      | `rowData.getIn(fieldPath)` used later in [columnDef getValue](#columndef-getvalue)
&nbsp;           | defaultValue      | any        | cell default value, returned by `getValue`
RowEmpty         | &nbsp;            | &nbsp;     | &nbsp;
&nbsp;           | DataTable props   |            | 

### getters

getter           | prop              | type       | description
-----------------|-------------------|------------|------------
getValue         |                   |            | 
&nbsp;           | value             | any        | 
&nbsp;           | fieldPath         | array      | 
&nbsp;           | rowData           | I.Map      | 
&nbsp;           | rowIndex          | integer    | row order bumber based on 0
&nbsp;           | DataTable props   |            | 
render           |                   |            | 
&nbsp;           | value             | any        | 
&nbsp;           | fieldPath         | array      | 
&nbsp;           | rowData           | I.Map      | 
&nbsp;           | rowIndex          | integer    | row order bumber based on 0
&nbsp;           | className         | string     | 
&nbsp;           | DataTable props   |            | 
getValueTh       |                   |            | returns title for table column
&nbsp;           | value             | any        | `columnDef.title` [column definition](#available-options-for-columns)
&nbsp;           | DataTable props   |            | 
renderTh         |                   |            | 
&nbsp;           | value             | any        | returned by `getValueTh`
&nbsp;           | className         | string     | 
&nbsp;           | DataTable props   |            | 




#### columnDef getValue
```jsx
let value = rowData.getIn(fieldPath);
value = getValue ? getValue({ value, columnDef, rowData, rowIndex, ...props });
```


#### columnsSelector
```jsx
// file ./columns.js
import { columnsSelector } from 'react-immutable-jss-data-table';
const COLUMNS_DEF = { title, link, user, clickMe, select: null };
COLUMNS_DEF.select = columnsSelector(COLUMNS_DEF);
export default COLUMNS_DEF;

// file with table
import DataTable from 'react-immutable-jss-data-table';
import tableColumns from './columns.js';
const columns = tableColumns.select('title', 'link', 'user', 'clickMe');

// in your component with table
<DataTable
  columns={columns}
  items={items}
  ...
/>
```


#### getColumnDef
```jsx
// file ./columns.js
import { getColumnDef } from 'react-immutable-jss-data-table';
export default const COLUMNS_DEF = {
  title, link, user, clickMe,
  select: (...keys) => keys.map(col => getColumnDef(COLUMNS_DEF, col)),
};

// file with table
import DataTable from 'react-immutable-jss-data-table';
import tableColumns from './columns.js';
const columns = tableColumns.select('title', 'link', 'user', 'clickMe');

// in your component with table
<DataTable
  columns={columns}
  items={items}
  ...
/>
```

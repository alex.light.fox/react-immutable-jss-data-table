import React from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import I from 'immutable';

import DataTable from '../DataTable';
import Row from '../Row';
import { setPropTypes } from '../helpers';

class CustomRowRenderer extends React.PureComponent {
  static propTypes = {
    rowData: PropTypes.instanceOf(I.Map).isRequired,
    rowIndex: PropTypes.number.isRequired,
    clickHandler: PropTypes.func.isRequired,
    foo: PropTypes.any,
  };
  render() {
    const { rowData, rowIndex, clickHandler, foo } = this.props;
    return (
      <div className="CustomRowRenderer">
        <h3>{`#${rowIndex}`} custom row renderer</h3>
        <div children={rowData.get('column1')} />
        <div children={rowData.get('column2')} />
        <div children={`foo is ${foo}`} />
        <button type="button" onClick={() => clickHandler({ rowIndex })} />
      </div>
    );
  }
}

describe('<DataTable />', () => {
  let defaultProps = {};
  let clickHandler;
  let columns;
  const items = I.fromJS([
    { id: 1, column1: 'R1 C1', column2: 'R1 C2', column3: 'R1 C3' },
    { id: 2, column1: 'R2 C1', column2: 'R2 C2', column3: 'R2 C3' },
  ]);
  beforeEach(() => {
    columns = [
      {
        fieldPath: ['column1'],
        className: 'tdColumn1',
        title: 'column 1',
      },
      {
        fieldPath: ['column2'],
        className: 'tdColumn2',
        title: 'column 2',
      },
      {
        fieldPath: ['column3'],
        className: 'tdColumn3',
        title: 'column 3',
      },
    ];
    clickHandler = jest.fn();
    defaultProps = {
      classes: {
        trEven: 'trEven',
        trOdd: 'trOdd',
        trh: 'trh',
        th: 'th',
        tr: 'tr',
        td: 'td',
        DataTable: 'DataTable',
        tdEmpty: 'tdEmpty',
        tdColumn1: 'column1-class-name',
      },
      items: items.slice(0, 1),
      columns,
      foo: 'bar',
      cellClasses: {
        tdColumn2: 'column2-class-name',
      },
      clickHandler,
    };
  });

  it('custom Row component', () => {
    const getRowRenderer = setPropTypes(props => {
      const { rowIndex, rowData } = props;
      const RowComponent = rowIndex === 1 || rowData.get('id') === 2 ? CustomRowRenderer : Row;
      return <RowComponent {...{ ...props, rowIndex, rowData }} />;
    }, {
      rowIndex: PropTypes.number,
      rowData: PropTypes.object,
    }, 'RowRenderer');
    const element = mount(
      <DataTable
        {...defaultProps}
        Row={getRowRenderer}
        items={items}
      />,
    );
    element.find('button').simulate('click');
    expect(clickHandler).toBeCalledWith({ rowIndex: 1 });
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('RowEmpty', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        items={I.List()}
        RowEmpty={() => '[[[ Empty row ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('showColumnsTitles', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        items={items}
        showColumnsTitles
        Row={() => '[[[ row ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('Cell component', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        items={items}
        Cell={() => '[[[ cell ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('Th', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        showColumnsTitles
        Th={() => '[[[ th ]]]'}
        Row={() => '[[[ row ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('className', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        className="table-class-name"
        Row={() => '[[[ row ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('getThClassNames', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        showColumnsTitles
        getThClassNames={() => 'th-class-custom'}
        Row={() => '[[[ row ]]]'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('getCellClassNames', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        showColumnsTitles
        getCellClassNames={() => 'cell-class-custom'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('getRowClassNames', () => {
    const element = mount(
      <DataTable
        {...defaultProps}
        showColumnsTitles
        getRowClassNames={() => 'row-class-custom'}
      />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

});

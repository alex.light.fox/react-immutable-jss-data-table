import React from 'react';
import PropTypes from 'prop-types';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';
import Th from '../Th';


describe('<Th />', () => {

  it('uses Th.title', () => {
    const columnDef = {};

    const defaultProps = {
      classes: {
        th: 'th',
      },
      title: 'Th title val',
      columnDef,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('uses columnDef.className', () => {
    const columnDef = {
      className: 'thClassName',
    };

    const defaultProps = {
      classes: {
        th: 'th',
      },
      title: 'Th title val',
      columnDef,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('uses classes[columnDef.className]', () => {
    const columnDef = {
      className: 'thClassName',
    };

    const defaultProps = {
      classes: {
        th: 'th',
        thClassName: 'thClassName-from-classes',
      },
      title: 'Th title val',
      columnDef,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('uses cellClasses[columnDef.className]', () => {
    const columnDef = {
      className: 'thClassName',
    };

    const defaultProps = {
      classes: {
        th: 'th',
        thClassName: 'thClassName-from-classes',
      },
      title: 'Th title val',
      cellClasses: { thClassName: 'thClassName-from-cellClasses' },
      columnDef,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('custom getValueTh with Th.title', () => {
    const columnDef = {
      title: 'columnDef title val',
      getValueTh: ({ value }) => `changed getValueTh: "${value}"`,
    };

    const defaultProps = {
      classes: {
        th: 'th',
      },
      columnDef,
      title: 'Th title val',
      getValueTh: columnDef.getValueTh,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });

  it('uses renderTh and pass value to it from custom getValueTh', () => {
    function ThCustom({ value, className }) {
      return <b className={className}>[[[ {value} ]]]</b>;
    }
    ThCustom.propTypes = {
      value: PropTypes.node,
      className: PropTypes.string,
    };
    const columnDef = {
      title: 'columnDef title val',
      getValueTh: ({ value }) => `changed getValueTh: "${value}"`,
      renderTh: ThCustom,
    };

    const defaultProps = {
      classes: {
        th: 'th',
      },
      title: 'Th title val',
      columnDef,
      renderTh: columnDef.renderTh,
      getValueTh: columnDef.getValueTh,
    };
    const element = mount(
      <Th {...defaultProps} />,
    );
    expect(toJson(element.render())).toMatchSnapshot();
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
// components
import { isValidReactComponent, getThClassNames } from './helpers';

export function getValueTh({ value }) {
  return value;
}

getValueTh.propTypes = {
  value: PropTypes.node,
};

export function renderTh({ className, value }) {
  return (
    <div
      className={className}
      children={value}
    />
  );
}

renderTh.propTypes = {
  className: PropTypes.string.isRequired,
  value: PropTypes.node,
};

class Th extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.shape({
      th: PropTypes.string.isRequired,
    }).isRequired,
    title: isValidReactComponent,
    getValueTh: isValidReactComponent,
    renderTh: isValidReactComponent,
    getThClassNames: PropTypes.func,
  };

  static defaultProps = {
    title: '',
    getValueTh,
    renderTh,
    getThClassNames,
  };

  render() {
    const { getValueTh: Value, renderTh: Th, title, getThClassNames } = this.props;
    const value = <Value {...{ ...this.props, value: title }} />;
    const classNames = getThClassNames(this.props);
    return <Th
      {...{ ...this.props, value, className: cx(classNames) }}
    />;
  }
}

export default Th;

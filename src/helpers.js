import { isValidElementType } from 'react-is';

class NoColumnDefinition extends Error {};

export const setPropTypes = (f, propTypes, displayName) => Object.assign(f, { propTypes, displayName });

export const getColumnDef = (columns, col) => {
  const columnDef = columns[col];
  if (columnDef) return { ...columnDef, name: columnDef.name || col };
  const err = new NoColumnDefinition(`There is no column definition «${col}» in columns map`);
  err.data = { columns, col };
  throw err;
};

export const columnsSelector = (columns) => (...keys) =>
  keys.map(col => getColumnDef(columns, col));

export const getCellClassNamesFactory = baseKey => props => {
  const { classes, cellClasses = {}, columnDef } = props;
  const ret = [classes[baseKey]];
  if (columnDef.className) {
    const key = columnDef.className;
    ret.push(cellClasses[key] || classes[key] || key);
  }
  return ret;
};

export const getCellClassNames = getCellClassNamesFactory('td');
export const getThClassNames = getCellClassNamesFactory('th');

export const getRowClassNames = (props) => {
  const { classes, rowIndex } = props;
  let rowClassNames = [classes.tr];
  // skip tr with headers
  if (isFinite(rowIndex)) {
    rowClassNames.push((rowIndex % 2) ? classes.trEven : classes.trOdd);
  }
  return rowClassNames;
};

export const NoDataExtractor = new Error('columns definition should specify `fieldPath` or `getValue` or `render`');

export const isValidReactComponent = (props, propName, componentName) => {
  if (props[propName] && !isValidElementType(props[propName])) {
    return new Error(`Invalid prop "${propName}" supplied to "${componentName}."`);
  }
  return null;
};

isValidReactComponent.isRequired = (props, propName, componentName) => {
  if (!props[propName]) {
    return new Error(`Prop "${propName}" is required for "${componentName}."`);
  }
  return isValidReactComponent(props, propName, componentName);
};

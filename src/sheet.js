const DEFAULT_FONTFAMILY = 'Arial, Helvetica, sans-serif';

export default {
  DataTable: {
    // root
    background: '#FFF',
    boxShadow: '0 2px 8px rgba(0, 0, 0, 0.2)',
  },
  tr: {
    display: 'flex',
    alignItems: 'stretch',
    padding: [0],
    borderBottom: '1px solid #FFF',
    minHeight: 53,
    font: `400 13px/18px ${DEFAULT_FONTFAMILY}`,
  },
  th: {
    flex: '1 1 0',
    font: `500 13px/18px ${DEFAULT_FONTFAMILY}`,
  },
  td: {
    flex: '1 1 0',
    boxSizing: 'border-box',
    // more
    color: '#000',
    font: `400 13px/18px ${DEFAULT_FONTFAMILY}`,
    alignSelf: 'center',
  },
  trEven: {
    backgroundColor: '#f7f7f7',
  },
  trOdd: {},
  tdEmpty: {},
  trh: {
    borderBottom: '2px solid #a9431e',
    alignItems: 'center',
  },
};

import React from 'react';
import PropTypes from 'prop-types';
import I from 'immutable';
import cx from 'classnames';
import { NoDataExtractor, isValidReactComponent, getCellClassNames } from './helpers';

export function getValue({ value, defaultValue = null }) {
  if (typeof value === 'undefined') return defaultValue;
  return value;
}

getValue.propTypes = {
  value: PropTypes.node,
  fieldPath: PropTypes.array,
  defaultValue: PropTypes.any,
};

export function renderCell({ value, className }) {
  return <div
    className={className}
    children={value}
  />;
}

renderCell.propTypes = {
  value: PropTypes.node,
  className: PropTypes.string.isRequired,
};

class Cell extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.shape({
      td: PropTypes.string.isRequired,
    }).isRequired,
    rowData: PropTypes.instanceOf(I.Collection).isRequired,
    fieldPath: PropTypes.array,
    render: isValidReactComponent,
    getValue: isValidReactComponent,
    getCellClassNames: PropTypes.func.isRequired,
    value: PropTypes.node,
  };

  static defaultProps = {
    render: renderCell,
    getCellClassNames,
  };

  render() {
    const { rowData, getValue: Value, render: Cell, fieldPath, getCellClassNames } = this.props;
    if (!fieldPath && !Value && !Cell) {
      throw NoDataExtractor;
    }
    let value = this.props.value;
    if (Value) {
      value = <Value
        {...{
          ...this.props,
          value: fieldPath ? rowData.getIn(fieldPath) : void 0,
        }}
      />;
    }
    return <Cell
      {...{
        ...this.props,
        value,
        className: cx(getCellClassNames(this.props)),
      }}
    />;
  }
}

export default Cell;

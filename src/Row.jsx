import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import I from 'immutable';
import { isValidReactComponent, getRowClassNames } from './helpers';

class Row extends React.PureComponent {
  static propTypes = {
    getRowClassNames: PropTypes.func,
    columns: PropTypes.array.isRequired,
    rowIndex: PropTypes.number.isRequired,
    rowData: PropTypes.oneOfType([
      PropTypes.instanceOf(I.Record),
      PropTypes.instanceOf(I.List),
      PropTypes.instanceOf(I.Map),
      PropTypes.instanceOf(I.Collection),
    ]).isRequired,
    classes: PropTypes.object.isRequired,
    items: PropTypes.instanceOf(I.Collection).isRequired,
    Cell: isValidReactComponent.isRequired,
    render: isValidReactComponent,
    getValue: isValidReactComponent,
    defaultValue: PropTypes.any,
  };

  static defaultProps = {
    getRowClassNames,
  };

  render() {
    const { columns, Cell, getRowClassNames, rowData } = this.props;
    const classNames = getRowClassNames(this.props);
    return (
      <div
        className={cx(classNames)}
        role="row"
      >
        {
          columns.map((columnDef, key) => (
            <Cell
              key={`${key}-${columnDef.name}`}
              {...{ columnDef, ...this.props }}
              render={columnDef.render || this.props.render}
              getValue={columnDef.getValue || this.props.getValue}
              defaultValue={columnDef.defaultValue || this.props.defaultValue}
              fieldPath={columnDef.fieldPath}
              value={columnDef.fieldPath ? rowData.getIn(columnDef.fieldPath) : void 0}
            />
          ))
        }
      </div>
    );
  }
}

export default Row;

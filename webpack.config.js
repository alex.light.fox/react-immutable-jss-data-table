var path = require('path');

module.exports = {
    mode: 'production',
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'index.js',
        libraryTarget: 'commonjs2',
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                include: path.resolve(__dirname, 'src'),
                exclude: /(node_modules|bower_components|build)/,
                use: 'babel-loader',
            },
        ],
    },
    optimization: {
        // minimize: false,
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'src'),
        ],
        extensions: [".js", ".jsx"],
    },
    externals: {
        // don't export React and other dependencies, use parent-project modules instead
        // 'react': 'commonjs react',
        react: 'react',
        classnames: 'classnames',
        // immutable: 'immutable',
        immutable: {
            commonjs: 'immutable',
            commonjs2: 'immutable',
            amd: 'immutable',
            root: 'immutable',
        },
        'prop-types': 'prop-types',
        'react-is': 'react-is',
    },
};

module.exports = {
  setupTestFrameworkScriptFile: './src/setupTests.js',
  watchPathIgnorePatterns: [
    '/yarn-error\\.log/',
    '/yarn\\.lock/',
    '/diff\\.diff/',
    '/node_modules/',
    '/.git/',
  ],
};
